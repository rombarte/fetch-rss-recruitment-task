<?php

declare(strict_types=1);

namespace BartlomiejRomanekRekrutacjaHRtec\Service;

use PHPUnit\Framework\TestCase;

class CliArgumentsParserTest extends TestCase
{
    /**
     * @runTestsInSeparateProcesses
     */
    public function testParse()
    {
        $actual = CliArgumentsParser::parse([
            'console.php',
            'csv:simple',
            'www.cornhub.com/feed.xml',
            'file.csv',
        ]);
        $expected = [
            0 => 'console.php',
            1 => 'csv:simple',
            2 => 'www.cornhub.com/feed.xml',
            3 => 'file.csv',
            'command' => 'csv:simple',
        ];
        self::assertEquals($expected, $actual);
        self::assertEquals(4, CliArgumentsParser::getArgumentCount());
    }
}
