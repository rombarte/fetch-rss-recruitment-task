<?php

declare(strict_types=1);

namespace BartlomiejRomanekRekrutacjaHRtec\Service;

use PHPUnit\Framework\TestCase;

class HtmlTextConverterTest extends TestCase
{
    public function testRemoveEntities()
    {
        $converter = new HtmlTextConverter();
        $actual = $converter->removeEntities('<b>The Witcher 3: Wild Hunt</b>');
        self::assertEquals('The Witcher 3: Wild Hunt', $actual);

        $actual = $converter->removeEntities(
            '<a href="/wiki/Action-Rollenspiel" title="Action-Rollenspiel">Action-Rollenspiel</a>'
        );
        self::assertEquals('Action-Rollenspiel', $actual);
    }
}
