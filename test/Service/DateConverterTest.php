<?php
declare(strict_types=1);

namespace BartlomiejRomanekRekrutacjaHRtec\Service;

use PHPUnit\Framework\TestCase;

class DateConverterTest extends TestCase
{
    public function testConvertToFullDateFromDateOnly()
    {
        $converter = new DateConverter();
        $actual = $converter->convertToFullDate('11 November 2005');
        self::assertEquals('11 listopada 2005 00:00:00 UTC', $actual);
    }

    public function testConvertToFullDateFromDateAndTime()
    {
        $converter = new DateConverter();
        $actual = $converter->convertToFullDate('9 November 2018, 9:00 pm');
        self::assertEquals('9 listopada 2018 21:00:00 UTC', $actual);
    }
}
