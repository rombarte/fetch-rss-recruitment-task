# Fetch RSS - Recruitment Task
Simple RSS/Atom fetcher

1. Check system requirements: `composer check`.
2. Check code sniffer validation: `.\vendor\bin\phpcs -p`
3. Check unit tests validation: `.\vendor\bin\phpunit`
4. Run app: `php src/console.php`

Source code is distributed on MIT License.