<?php

declare(strict_types=1);

namespace BartlomiejRomanekRekrutacjaHRtec\Service;

use BartlomiejRomanekRekrutacjaHRtec\Core\Service\IDateConverter;

class DateConverter implements IDateConverter
{
    /**
     * @var \IntlDateFormatter
     */
    private $intlDateFormatter;

    /**
     * DateConverter constructor.
     * @param string $language
     */
    public function __construct(string $language = 'pl_PL')
    {
        $this->intlDateFormatter = new \IntlDateFormatter(
            $language,
            \IntlDateFormatter::LONG,
            \IntlDateFormatter::LONG
        );
    }

    /**
     * @param string $date
     * @return string
     */
    public function convertToFullDate(string $date): string
    {
        $dateObject = new \DateTime($date);
        return $this->intlDateFormatter->format($dateObject);
    }
}
