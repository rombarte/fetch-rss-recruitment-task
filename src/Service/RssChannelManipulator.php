<?php

declare(strict_types=1);

namespace BartlomiejRomanekRekrutacjaHRtec\Service;

class RssChannelManipulator extends \SimplePie
{
    /**
     * @var string
     */
    private $url;

    /**
     * @throws \UnexpectedValueException
     * @throws \SimplePie_Exception
     */
    private function prepareConnection(): void
    {
        if ($this->url === null) {
            throw new \UnexpectedValueException('Specified feed url is empty');
        }
        $this->set_feed_url($this->url);
        $this->init();
        if (null !== $this->error()) {
            throw new \SimplePie_Exception('Can\'t connect to choosen feed');
        };
    }

    /**
     * @param string $url
     * @return RssChannelManipulator
     */
    public function setUrl(string $url): RssChannelManipulator
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return \SimplePie_Item[]
     * @throws \SimplePie_Exception
     */
    public function getItems(): array
    {
        $this->prepareConnection();
        return $this->get_items();
    }
}
