<?php

declare(strict_types=1);

namespace BartlomiejRomanekRekrutacjaHRtec\Service;

class CliArgumentsParser
{
    /**
     * @var int
     */
    private static $argumentCount = 0;

    /**
     * @param array $cliArguments
     * @return array
     */
    public static function parse(array $cliArguments): array
    {
        self::$argumentCount = count($cliArguments);
        $cliArguments['command'] = $cliArguments[1] ?? '';
        return $cliArguments;
    }

    /**
     * @return int
     */
    public static function getArgumentCount(): int
    {
        return self::$argumentCount;
    }
}
