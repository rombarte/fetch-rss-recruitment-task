<?php

declare(strict_types=1);

namespace BartlomiejRomanekRekrutacjaHRtec\Service;

use BartlomiejRomanekRekrutacjaHRtec\Core\Service\IHtmlTextConverter;

class HtmlTextConverter implements IHtmlTextConverter
{
    /**
     * @param string $text
     * @return string
     */
    public function removeEntities(string $text): string
    {
        return strip_tags($text);
    }
}
