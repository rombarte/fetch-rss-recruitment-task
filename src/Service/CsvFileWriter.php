<?php

declare(strict_types=1);

namespace BartlomiejRomanekRekrutacjaHRtec\Service;

use BartlomiejRomanekRekrutacjaHRtec\Core\Service\IDateConverter;
use BartlomiejRomanekRekrutacjaHRtec\Core\Service\IHtmlTextConverter;
use League\Csv\Writer;

class CsvFileWriter
{
    /**
     * @var \SimplePie_Item[]
     */
    private $items;

    /**
     * @var IDateConverter
     */
    private $dateConverter;

    /**
     * @var IHtmlTextConverter
     */
    private $htmlTextConverter;

    /**
     * CsvFileWriter constructor.
     * @param IDateConverter $dateConverter
     * @param IHtmlTextConverter $htmlTextConverter
     */
    public function __construct(IDateConverter $dateConverter, IHtmlTextConverter $htmlTextConverter)
    {
        $this->dateConverter = $dateConverter;
        $this->htmlTextConverter = $htmlTextConverter;
    }

    /**
     * @param array $rssItems
     * @return CsvFileWriter
     */
    public function setItems(array $rssItems): CsvFileWriter
    {
        $this->items = $rssItems;
        return $this;
    }

    /**
     * @return array
     */
    private function getPreparedData(): array
    {
        $outputItems = [];
        foreach ($this->items as $rssItem) {
            $author = $rssItem->get_author();
            $outputItem = [
                'title' => $rssItem->get_title(),
                'description' => $this->htmlTextConverter->removeEntities($rssItem->get_description()),
                'link' => $rssItem->get_link(),
                'pubDate' => $this->dateConverter->convertToFullDate($rssItem->get_date()),
                'creator' => $author !== null ? $author->get_name() : '',
            ];
            $outputItems[] = $outputItem;
        }
        return $outputItems;
    }

    /**
     * @return array
     */
    private function getPreparedHeader(): array
    {
        return ['title', 'description', 'link', 'pubDate', 'creator'];
    }

    /**
     * @param $path
     * @return string
     */
    private function parseFilePath($path): string
    {
        $fileInfo = pathinfo($path);
        if (!isset($fileInfo['extension']) || $fileInfo['extension'] !== 'csv') {
            $path .= '.csv';
        }
        return $path;
    }

    /**
     * @param string $outputFilePath
     * @param bool $appendFile
     * @throws \League\Csv\CannotInsertRecord
     */
    public function saveFile(string $outputFilePath, bool $appendFile = false): void
    {
        $outputFilePath = $this->parseFilePath($outputFilePath);
        $outputItems = $this->getPreparedData();
        if ($appendFile) {
            $csv = Writer::createFromPath($outputFilePath, 'a+');
        } else {
            $csv = Writer::createFromString('');
        }
        if ('' === $csv->getContent()) {
            $header = $this->getPreparedHeader();
            $csv->insertOne($header);
        }
        foreach ($outputItems as $outputItem) {
            $csv->insertOne($outputItem);
        }
        file_put_contents($outputFilePath, $csv->getContent());
    }
}
