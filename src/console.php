<?php

declare(strict_types=1);

namespace BartlomiejRomanekRekrutacjaHRtec;

use BartlomiejRomanekRekrutacjaHRtec\Core\Application;
use BartlomiejRomanekRekrutacjaHRtec\Core\Environment;

try {
    if (false === file_exists("vendor/autoload.php")) {
        throw new \Exception('Can\'t find autoload file. Run app from main directory');
    }
    include_once("vendor/autoload.php");
    Environment::prepare();
    Application::$cliArguments = $argv;
    Application::run();
} catch (\Exception $exception) {
    echo 'Error: ' . $exception->getMessage();
    exit(1);
}
