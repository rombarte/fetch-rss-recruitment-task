<?php

declare(strict_types=1);

namespace BartlomiejRomanekRekrutacjaHRtec\Core;

use BartlomiejRomanekRekrutacjaHRtec\Service\CliArgumentsParser;
use BartlomiejRomanekRekrutacjaHRtec\Service\CsvFileWriter;
use BartlomiejRomanekRekrutacjaHRtec\Service\DateConverter;
use BartlomiejRomanekRekrutacjaHRtec\Service\HtmlTextConverter;
use BartlomiejRomanekRekrutacjaHRtec\Service\RssChannelManipulator;

class Application
{
    private const APP_RUN_MODE_SIMPLE = 'csv:simple';
    private const APP_RUN_MODE_EXTENDED = 'csv:extended';

    /**
     * @var array
     */
    public static $cliArguments = [];

    /**
     * @param string $source
     * @param string $destinationFile
     * @param bool $append
     * @throws \League\Csv\CannotInsertRecord
     * @throws \SimplePie_Exception
     */
    private static function csv(string $source, string $destinationFile, bool $append = false): void
    {
        $rssChannel = new RssChannelManipulator();
        $rssChannel->setUrl($source);
        $rssItems = $rssChannel->getItems();

        $csvFile = new CsvFileWriter(
            new DateConverter(),
            new HtmlTextConverter()
        );
        $csvFile->setItems($rssItems);
        $csvFile->saveFile($destinationFile, $append);
    }

    /**
     * @return int
     * @throws \League\Csv\CannotInsertRecord
     * @throws \SimplePie_Exception
     */
    public static function run(): int
    {
        $appSettings = CliArgumentsParser::parse(self::$cliArguments);

        switch ($appSettings['command']) {
            case self::APP_RUN_MODE_SIMPLE:
                $minimumCommandArguments = 4;
                if (CliArgumentsParser::getArgumentCount() < $minimumCommandArguments) {
                    throw new \InvalidArgumentException(
                        'Please specify all required arguments: feed_url and csv_file_name'
                    );
                }
                self::csv($appSettings[2], $appSettings[3]);
                break;
            case self::APP_RUN_MODE_EXTENDED:
                $minimumCommandArguments = 4;
                if (CliArgumentsParser::getArgumentCount() < $minimumCommandArguments) {
                    throw new \InvalidArgumentException(
                        'Please specify all required arguments: feed_url and csv_file_name'
                    );
                }
                self::csv($appSettings[2], $appSettings[3], true);
                break;
            default:
                throw new \InvalidArgumentException('Command not found. Allowed commands: csv:simple, csv:extended');
                break;
        }

        exit(0);
    }
}
