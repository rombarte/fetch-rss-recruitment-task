<?php

declare(strict_types=1);

namespace BartlomiejRomanekRekrutacjaHRtec\Core\Service;

interface IDateConverter
{
    /**
     * @param string $date
     * @return string
     */
    public function convertToFullDate(string $date): string;
}
