<?php

declare(strict_types=1);

namespace BartlomiejRomanekRekrutacjaHRtec\Core\Service;

interface IHtmlTextConverter
{
    /**
     * @param string $text
     * @return string
     */
    public function removeEntities(string $text): string;
}
