<?php

declare(strict_types=1);

namespace BartlomiejRomanekRekrutacjaHRtec\Core;

class Environment
{
    /**
     * Prepare app environment to work
     */
    public static function prepare(): void
    {
        /* Cache directory is necessary for SimplePie library */
        if (!file_exists('cache')) {
            mkdir('cache');
        }
    }
}
